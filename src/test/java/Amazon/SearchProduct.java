package Amazon;

import org.testng.annotations.Test;

import Amazon.Assessment.AddtoCart;
import Amazon.Assessment.AmazonCart;
import Amazon.Assessment.HomePage;

import org.testng.annotations.BeforeTest;

import static org.testng.Assert.assertEquals;

import java.time.Duration;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class SearchProduct {
	WebDriver driver;
	String BeforeCartName;
	HomePage obj;
	AddtoCart obj1;
	AmazonCart obj2;
	String s ="";
	@BeforeTest
	public void initialize() {
		
		obj= new HomePage(driver);
		  driver.get("https://www.amazon.in/");
		  driver.manage().window().maximize();
	}
  @Test(priority=1)
  public void selectItem() {
	  
	  
	//  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
	  obj.searchTextBox().sendKeys("iphone x"+Keys.ENTER);
	   BeforeCartName=obj.Product().getText();
	  obj.Product().click();
	  Set<String> windows=driver.getWindowHandles();
	  for(String id:windows) {
		  driver.switchTo().window(id);
		 
	  }
    	  
  }
  @Test(dependsOnMethods= {"selectItem"})
	 public void addToCart() {
	  obj1=new AddtoCart(driver);
	  driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20));
	  obj1.AddToCart().click();
	  String AfterCartName= obj1.ProductVerify().getText();
	 
	  WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(30) );
	  wait.until(ExpectedConditions.visibilityOf(obj1.Cart())); 
	  wait.until(ExpectedConditions.elementToBeClickable(obj1.Cart()));
	  obj1.Cart().click();
	 
	assertEquals(BeforeCartName, AfterCartName);
	  
  }
  
  @Test(priority=3)
   public void AddItems() throws InterruptedException {
	  obj2=new AmazonCart(driver);
	  obj2.Quantity().click();
	  obj2.Number().click();
	  obj2.Textbox().sendKeys("10");
	  obj2.Update().click();
	  
	  WebDriverWait wait=new WebDriverWait(driver, Duration.ofSeconds(30));
	  Thread.sleep(2000);
	  wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@id='sc-subtotal-amount-activecart']/span")));
	 //System.out.println(obj.SinglePrice().getText());
	 String sp=obj2.SinglePrice().getText();
	sp.replaceAll("[^0-9]", "");
	 
	 String wp=obj2.WholePrice().getText();
	 wp.replaceAll("[^0-9]", "");
	 
	// System.out.println(sp);
	  int sp1= Integer.parseInt(sp) ;
	 int wp1=Integer.parseInt(wp);
	System.out.println(sp);
	System.out.println(wp);
	Assert.assertEquals(sp1*2,wp1);
  }
  @Test(priority=4)
  public void RemoveItems() {
	  AmazonCart obj2=new AmazonCart(driver);
	  obj2.Delete().click();
	  WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
		wait.until(ExpectedConditions.visibilityOfElementLocated(obj2.Verify));
	  Assert.assertEquals(obj2.Verify().getText(),"Your Amazon Cart is empty.");
	  
  }
  @BeforeSuite
  public void beforeTest() {
	  System.setProperty("webdriver.chrome.driver","C:\\Users\\vuppa\\Assessment\\chromedriver.exe" );
	  driver=new ChromeDriver();
	  	  
  }

  @AfterSuite
  public void afterTest() {
	//  driver.quit();
  }

}
