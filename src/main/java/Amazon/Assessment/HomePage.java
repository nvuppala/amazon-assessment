package Amazon.Assessment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
WebDriver driver1;
//String s;
public HomePage(WebDriver driver) {
	this.driver1=driver;
}
By searchTextBox=By.xpath("//input[@type='text']");
By Product=By.xpath("(//a[@class='a-link-normal s-underline-text s-underline-link-text s-link-style a-text-normal'])");


public WebElement searchTextBox() {
	return driver1.findElement(searchTextBox);
}

public WebElement Product() {
	return driver1.findElement(Product);
	}


}

