package Amazon.Assessment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AddtoCart {
	WebDriver driver1;
	public AddtoCart(WebDriver driver) {
		this.driver1=driver;
	}
	By AddToCart =By.xpath("//input[@id='add-to-cart-button']");
	By GoToCart =By.xpath("//a[@class='a-button-text']");
	By Cart=By.xpath("(//span[@id='attach-sidesheet-view-cart-button'])[1]");
	By Quantity=By.xpath("//span[@data-a-class='quantity']");
	By ProductVerify=By.xpath("//span[@id='productTitle']");
	
	public WebElement AddToCart() {
		return driver1.findElement(AddToCart);
	}
	public WebElement Cart() {
		return driver1.findElement(Cart);
	}
	public WebElement ProductVerify() {
		return driver1.findElement(ProductVerify);
	}
	
}
