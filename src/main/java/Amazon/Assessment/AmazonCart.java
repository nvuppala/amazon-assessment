package Amazon.Assessment;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AmazonCart {
	WebDriver driver1;
	public AmazonCart(WebDriver driver) {
		this.driver1=driver;
	}
	By Quantity=By.xpath("//span[@data-a-class='quantity']");
	By Number=By.xpath("//li[@aria-labelledby='quantity_10']");
	By Textbox = By.xpath("(//input[@type='text'])[2]");
	By Update=By.xpath("//a[@data-action='update']");
	By SinglePrice=By.xpath("//p[@class='a-spacing-mini']/span");
	By WholePrice= By.xpath("//span[@id='sc-subtotal-amount-activecart']/span");
	By Delete =By.xpath("//input[@value='Delete']");
	public By Verify =By.xpath("//h1[@class='a-spacing-mini a-spacing-top-base']");
	public WebElement Quantity() {
		return driver1.findElement(Quantity);
	}
	public WebElement Number() {
		return driver1.findElement(Number);
	}
	public WebElement Textbox() {
		return driver1.findElement(Textbox);
	}
	public WebElement Update() {
		return driver1.findElement(Update);
	}
	public WebElement SinglePrice() {
		return driver1.findElement(SinglePrice);
	}
	public WebElement WholePrice() {
		return driver1.findElement(WholePrice);
	}
	public WebElement Delete() {
		return driver1.findElement(Delete);
	}
	public WebElement Verify() {
		return driver1.findElement(Verify);
	}
}
